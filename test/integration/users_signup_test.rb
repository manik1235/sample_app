require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information doesn't create a new user record" do
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "",
                                         email: "user@invalid",
                                         password: "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
  end

  test "valid signup information creates a new user record" do
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name: "Testy Testerson",
                                         email: "user@example.com",
                                         password: "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
  end

  test "a blank name shows the `name can't be blank` error message" do
    post users_path, params: { user: { name: "",
                                       email: "user@invalid",
                                       password: "foo",
                                       password_confirmation: "bar" } }

    assert_includes @response.body, "Name can&#39;t be blank"
  end

  test "a blank password shows the `password can't be blank` error message" do
    post users_path, params: { user: { name: "Andrew",
                                       email: "user@invalid",
                                       password: "",
                                       password_confirmation: "bar" } }

    assert_includes @response.body, "Password can&#39;t be blank"
  end

  test "a short password shows the `password is too short` error message" do
    post users_path, params: { user: { name: "Andrew",
                                       email: "user@invalid",
                                       password: "foo",
                                       password_confirmation: "bar" } }

    assert_includes @response.body, "Password is too short"
  end

  test "a blank email shows the `email can't be blank` error message" do
    post users_path, params: { user: { name: "Andrew",
                                       email: "",
                                       password: "foo",
                                       password_confirmation: "bar" } }

    assert_includes @response.body, "Email can&#39;t be blank"
  end

  test "an invalid email shows the `email is invalid` error message" do
    post users_path, params: { user: { name: "Andrew",
                                       email: "user@invalid",
                                       password: "foo",
                                       password_confirmation: "bar" } }

    assert_includes @response.body, "Email is invalid"
  end
end
